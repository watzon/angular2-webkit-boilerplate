import {browser, element, by, By, $, $$, ExpectedConditions} from 'protractor/globals';

describe('App component', () => {

    it('should work', () => {
        browser.get('http://localhost:8080');
        expect(element(by.tagName('img')).isPresent()).toBeTruthy();
    });
    
});
