'use strict';

const path = require('path');
const express = require('express');

module.exports = {
  app: function () {
    const app = express();
    const indexPath = express.static(path.join(__dirname, 'dist'));
    const publicPath = express.static(path.join(__dirname, 'public'));

    app.use('/public', publicPath)
    app.get('/', indexPath);

    return app;
  }
}