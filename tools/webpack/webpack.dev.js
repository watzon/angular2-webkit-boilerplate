'use strict';

const webpackMerge = require('webpack-merge');
const ExtractTextPlugin = require('extract-text-webpack-plugin');
const commonConfig = require('./webpack.common.js');
const webpack = require('webpack');
const helpers = require('../helpers');

let hotMiddlewareScript = 'webpack-hot-middleware/client?path=/__webpack_hmr&timeout=20000&reload=true';

module.exports = webpackMerge(commonConfig, {
  devtool: 'cheap-module-eval-source-map',

  entry: {
    'polyfills': [helpers.root('src/polyfills.ts'), hotMiddlewareScript],
    'vendor': [helpers.root('src/vendor.ts'), hotMiddlewareScript],
    'app': [helpers.root('src/main.ts'), hotMiddlewareScript]
  },

  output: {
    path: helpers.root('dist'),
    publicPath: 'http://localhost:8080/',
    filename: '[name].js',
    chunkFilename: '[id].chunk.js'
  },

  plugins: [
    new ExtractTextPlugin('[name].css'),
    new webpack.HotModuleReplacementPlugin(),
    new webpack.NoErrorsPlugin()
  ],

  devServer: {
    historyApiFallback: true,
    stats: 'minimal'
  }
});
