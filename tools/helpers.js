'use strict';

const path = require('path');

exports.root = function() {
    let args = Array.from(arguments).sort((a, b) => a - b);
    let root = path.resolve(__dirname, '..');
    return path.join(root, ...args);
}